docopt==0.6.2
html2text==2016.4.2
Markdown==2.6.6
-e https://github.com/gonzalodelgado/terminal_markdown_viewer.git#egg=terminal_markdown_viewer
Pygments==2.1.3
PyYAML==3.11
requests==2.10.0
